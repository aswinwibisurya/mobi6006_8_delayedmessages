package com.example.aswin.delayedmessages;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

public class DelayedMessageService extends Service {
    public static final String ACTION_SHOW_MESSAGE = "com.example.aswin.delayedmessages.DelayedMessageService.action.showmessage";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.getAction().equals(ACTION_SHOW_MESSAGE)) {
            String param = intent.getStringExtra("param");

            synchronized (this) {
                try {
                    wait(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            String message = "Service started with param: " + param;

            Log.v("DelayedMessageService", message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

            stopSelf();
        }

        //return START STICKY --> if service is killed before finished, start again with null intent
        //return START_NOT_STICKY --> if service is killed, let it be.
        //return START_REDELIVER_INTENT --> if service is killed before finished, start again with the same intent
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
