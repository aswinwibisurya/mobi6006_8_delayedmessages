package com.example.aswin.delayedmessages;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import java.util.logging.LogRecord;

public class DelayedMessageIntentService extends IntentService {
    public static final String ACTION_SHOW_MESSAGE = "com.example.aswin.delayedmessages.DelayedMessageIntentService.action.showmessage";

    private Handler handler;

    public DelayedMessageIntentService() {
        super("DelayedMessageIntentService");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        handler = new Handler();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SHOW_MESSAGE.equals(action)) {
                final String param = intent.getStringExtra("param");

                synchronized (this) {
                    try {
                        wait(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                final String message = "Service started with param: " + param;

                //The Toast below will not show up. Why??
                //Answer: because it is run on a worker thread.
                //needs handler to run on main thread.
//                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String message = "Service started with param: " + param;
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }
}
