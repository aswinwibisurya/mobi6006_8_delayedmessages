package com.example.aswin.delayedmessages;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompatExtras;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvProgress;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    public static int NOTIFY_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvProgress = findViewById(R.id.tvProgress);
    }

    public void clickStartService(View view) {
        //using service
//        Intent intent = new Intent(this, DelayedMessageService.class);
//        intent.setAction(DelayedMessageService.ACTION_SHOW_MESSAGE);
//        intent.putExtra("param", "hello");
//        startService(intent);

        //using intent service
        Intent intent = new Intent(this, DelayedMessageIntentService.class);
        intent.setAction(DelayedMessageIntentService.ACTION_SHOW_MESSAGE);
        intent.putExtra("param", "hello");
        startService(intent);
    }

    public void clickStopService(View view) {
        //only for service. an Intent service finishes by itself after the tasks are finished.
        stopService(new Intent(this, DelayedMessageService.class));
    }

    public void startDownload(View view) {
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute("1.jpg", "2.jpg", "3.jpg");
    }


    //AsyncTask is used to perform background task, still related to a UI activity.
    //if activity is destroyed, AsyncTask will also be destroyed
    class DownloadTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... strings) {
            int count = strings.length;
            for(int i=0;i<strings.length; i++)  {
                //dummy download
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i+1, count);
            }
            return "Done";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int fileNum = values[0];
            int count = values[1];
            tvProgress.setText(String.format("Downloaded %d of %d", fileNum, count));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            tvProgress.setText(result);


            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);

                Notification.Builder notifBuilder = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Download Notification")
                        .setContentText("Download complete")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setChannelId(NOTIFICATION_CHANNEL_ID);

                notificationManager.notify(NOTIFY_ID, notifBuilder.build());
            } else {
                Notification.Builder notifBuilder = new Notification.Builder(getApplicationContext())
                        .setContentTitle("Download Notification")
                        .setContentText("Download complete")
                        .setSmallIcon(R.drawable.ic_launcher_background);

                notificationManager.notify(NOTIFY_ID, notifBuilder.build());
            }


        }
    }
}
